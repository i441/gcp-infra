variable "project_id" {
  description = "project id"
}
variable "region" {
  description = "The region to host the cluster in"
}
variable "zone" {
  description = "The zone to host the cluster in"
}
variable "credentials_account" {
  description = "The path to file with credentials json"
}
variable "network" {
  description = "The path to file with credentials json"
  default = "gke-network"
}
variable "subnetwork" {
  description = "The path to file with credentials json"
  default = "gke-network"
}
variable "env_name" {
  description = "The environment for the GKE cluster"
}
variable "cluster_name" {
  description = "The test k8s otus cluster"
}
variable "monitoring_service" {
  default     = "monitoring.googleapis.com/kubernetes"
}
variable "logging_service" {
  default     = "logging.googleapis.com/kubernetes"
}
variable "istio" {
  default     = ""
}
variable "http_load_balancing" {
  default     = "false"
}
variable "horizontal_pod_autoscaling" {
  default     = "false"
}
variable "initial_node_count" {
  default     = "1"
}
variable "daily_maintenance_window" {
}

# NODE_POOL_1
variable "name_node_pools_1" {
  description = "name of node_pools_1"
}
variable "node_pools_1_node_region" {
}
variable "node_pools_1_node_zone" {
}
variable "machine_type_node_pools_1" {
}
variable "node_pools_1_disk_size_gb" {
}
variable "node_pools_1_node_count" {
}
variable "node_pools_1_label" {
} 
variable "preemptible" {
  default     = "false"  
}

## AUTOSCALE
variable "node_pools_1_min_count_nodes" {
  default     = "3"
}
variable "node_pools_1_max_count_nodes" {
  default     = "3"
}

### MANAGEMENT
#variable "management_auto_repair" {
#  default     = "false"
#}
#variable "management_auto_upgrade" {
#  default     = "false"
#}