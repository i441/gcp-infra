module "gkecluster" {
  source                       = "../../modules/gkecluster"
  project_id                   = var.project_id
  region                       = var.region
  zone                         = var.zone
  credentials_account          = var.credentials_account
  env_name                     = var.env_name
  cluster_name                 = var.cluster_name
  network                      = var.network
#  subnetwork                   = var.subnetwork
  istio                        = var.istio
  http_load_balancing          = var.http_load_balancing
  horizontal_pod_autoscaling   = var.horizontal_pod_autoscaling
  daily_maintenance_window     = var.daily_maintenance_window
  initial_node_count           = var.initial_node_count
  name_node_pools_1            = var.name_node_pools_1
  node_pools_1_node_region     = var.node_pools_1_node_region
  node_pools_1_node_zone       = var.node_pools_1_node_zone
  machine_type_node_pools_1    = var.machine_type_node_pools_1
  node_pools_1_disk_size_gb    = var.node_pools_1_disk_size_gb
  node_pools_1_node_count      = var.node_pools_1_node_count
  node_pools_1_label           = var.node_pools_1_label
  preemptible                  = var.preemptible
  node_pools_1_min_count_nodes = var.node_pools_1_min_count_nodes
  node_pools_1_max_count_nodes = var.node_pools_1_max_count_nodes
  #    management_auto_upgrade         = "${var.management_auto_upgrade}"
  #    management_auto_repair          = "${var.management_auto_repair}"
}
